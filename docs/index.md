# Welcome to the Automotive SIG

This site contains general information about the Automotive SIG, as well as
how to contribute to the repository and how to build and download images.

The Automotive SIG manages several artifacts, including:

* Automotive Stream Distribution (AutoSD): This project is a binary
  distribution developed within the SIG that is a public,
  in-development preview of the upcoming Red Hat In-Vehicle
  Operating System (OS).
* Automotive SIG repositories: These are RPM repositories produced
  by the SIG to enhance the AutoSD. New packages or features can be
  developed and hosted there to expand the capabilities of the 
  AutoSD.
* Sample images: These are images built with [OSBuild](https://www.osbuild.org/)
  using packages from the AutoSD, the Automotive SIG repositories, or other 
  sources. They are examples of how to use AutoSD.

  <!--RHIVOS members have said the CentOS wiki is obsolete. Is there any value in pointing AutoSIG readers to the site?-->

For more information about the Automotive SIG charter, members, or goal, see the
[Automotive SIG page on the CentOS wiki](https://wiki.centos.org/SpecialInterestGroup/Automotive).

For Automotive SIG discussions, see [CentOS-Automotive-SIG mailing list](https://lists.centos.org/mailman/listinfo/centos-automotive-sig).
Or join the Matrix channel `#centos-automotive-sig`. For more information, see [centos-automotive-sig:fedoraproject.org](https://matrix.to/#/#centos-automotive-sig:fedoraproject.org).

## AutoSD

The AutoSD is an upstream repository
for Red Hat In-Vehicle OS, much like CentOS Stream is to RHEL.
AutoSD is based on CentOS Stream with a few divergences.

The first divergence is the Linux kernel. AutoSD relies on the kernel-automotive 
package rather than the CentOS Stream kernel package.

![Automotive Stream Distribution vs CentOS Stream](img/AutoSD_CS.jpg)

As a binary distribution, AutoSD is where the
community, customers, and partners can see what will land in
Red Hat In-Vehicle OS. Like CentOS Stream, AutoSD is 
open to contributions using similar mechanisms.

For more information about how AutoSD content
is gathered, see [Content Definition](content_definition.md).

### Downloading the repository

**Prerequisites**

* At least 1.6G of available disk space to accommodate
both `aarch64` and `x86_64`

!!! note

   The disk space requirement might change as the package set evolves.

* Download a local copy of the repository:

```
wget --recursive --no-parent -R "index.html*" 'https://autosd.sig.centos.org/AutoSD-9/nightly/repos/AutoSD/compose/AutoSD/'
```

!!! note

   The latest version of AutoSD is also available at
[https://autosd.sig.centos.org](https://autosd.sig.centos.org).


## Automotive SIG repositories

The Automotive SIG repositories include packages that are not
necessarily part of Red Hat In-Vehicle OS. These packages might be works in progress 
that could land in AutoSD or Red Hat In-Vehicle OS. 
Or the packages could be purely experimental--for research and
development--or integration work that remains outside of AutoSD.

All SIG members can request packages to be distributed through these
repositories,provided they meet [CentOS requirements for SIG](https://wiki.centos.org/SpecialInterestGroup#Requirements).

You can browse the Automotive SIG repositories at:
[https://buildlogs.centos.org/9-stream/automotive/](https://buildlogs.centos.org/9-stream/automotive/).

## Sample images

> **_IMPORTANT:_** Do not use sample images in production.

The [Automotive SIG](https://gitlab.com/centos/automotive/sample-images)
repository contains manifests for different operating systems and platforms.

The manifests are located in the `osbuild-manifests` folder.

```
    sample-images/
    └── osbuild-manifests
        └── images
```

For example:
```
    sample-images/
    └── osbuild-manifests
        └── images
           ├── minimal.mpp.yml
           ├── neptune.mpp.yml
           └── ...
```

The Automotive SIG uses [OSBuild](https://www.osbuild.org/) to build images.
For more information, see [Building images](building/index.md).

For information about pre-built images, see 
[Downloading images produced by the Automotive SIG](download_images.md).
