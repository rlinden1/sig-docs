# Building images

The Automotive SIG uses [OSBuild](https://www.osbuild.org/) as the tool to build
its images on CentOS Stream, Fedora, and RHEL hosts, with the option to build immutable images using [OSTree](https://ostreedev.github.io/ostree/introduction/).

## Prerequisites

- **No cross-compilation**: To build AArch64 or x86_64 images, you must run OSBuild on the respective systems. Some options for AArch64 hosting include Raspberry Pi 4 or qemu on Linux or macOS. For more information, see [AutoSD system running on a Raspberry Pi 4](autosd_pi4.md) or [AutoSD system running on qemu](autosd_qemu.md).

- **A subscribed RHEL system**: To build RHEL images, you must have access to a subscribed RHEL system in order to access to the entitlements. RHEL images can only be built on subscribed RHEL systems.

## Using the Automotive OSBuild Container Image

We have a container that can be used to build Automotive images with osbuild, if using the containerized environment, you can skip the installation steps in this document and proceed to to [Finding a manifest](#finding-a-manifest). The container build is maintained at [quay.io/centos-sig-automotive/automotive-osbuild](https://quay.io/repository/centos-sig-automotive/automotive-osbuild?tab=tags). Example usage:

```
git clone https://gitlab.com/CentOS/automotive/sample-images.git
sudo podman run -v $PWD:$PWD --rm --privileged --pull=newer --security-opt label=type:unconfined_t quay.io/centos-sig-automotive/automotive-osbuild /bin/bash -c "cd $PWD/sample-images/osbuild-manifests/; make cs9-qemu-minimal-ostree.x86_64.qcow2"
```

It is important that the git repo exists on the host as some commands cannot work from the containers overlayfs filesystem.

More information for the container is available in the [automotive-osbuild](https://gitlab.com/CentOS/automotive/container-images/automotive-osbuild) repository.

## Installing osbuild

To configure your system you must install osbuild and other related tools.

!!! note

    We use a newer version of these tools than what is generally available in the distributions, so you must configure a custom repository.

### CentOS Stream 8 only (one additional step)

0. Enable the osbuild repo:
    ```
    dnf copr enable @osbuild/osbuild-stable centos-stream-8-`uname -p`
    ```

### CentOS Stream 8/9, Fedora, or RHEL 8/9

1. Enable the osbuild repo:
    ```
    dnf copr enable @osbuild/osbuild-stable
    dnf copr enable @centos-automotive-sig/osbuild-auto
    ```
2. Install osbuild and related packages:
    ```
    dnf install osbuild osbuild-auto osbuild-ostree osbuild-tools
    ```

## Finding a manifest

All manifests exist in the project hosted at the [Automotive SIG GitLab](https://gitlab.com/CentOS/automotive/sample-images).

1. Clone the repository to download all manifests:
    ```
    git clone https://gitlab.com/CentOS/automotive/sample-images.git
    ```
2. Primary image manifests are in the `osbuild-manifests/images` folder.
    ```
    osbuild-manifests
    ├── images
    │   ├── composefs.mpp.yml
    │   ├── container.mpp.yml
    │   ├── containerperf.mpp.yml
    │   ├── developer.mpp.yml
    │   ├── directboot.csv
    │   ├── encrypted.mpp.yml
    │   ├── gadget.mpp.yml
    │   ├── minimal.mpp.yml
    │   ├── ocibased.mpp.yml
    │   ├── ocp.mpp.yml
    │   ├── qmcontainer.mpp.yml
    │   └── upgrade-demo.mpp.yml
    ```

## Building the image

You can build an image manually or by using makefile.

### Building the image manually

Building an image manually is a two-step process.

1. Pass the selected YAML manifest and some options to the `osbuild-mpp` tool. Example:
   ```
   osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D arch=\"aarch64\"
   ```
 
    This applies the options and resolves the package names against the repositories used to produce a JSON manifest with fully resolved versions of all the packages. This JSON file is fully self contained and produces reproducible builds.

    The manifest has multiple options that can affect how the manifest is preprocessed, for example:

    * `image_type` specifies whether the system is _ostree_ or _regular (dnf)_ based.
    * `extra_rpms` passes a list object of extra RPMs to the built image.

    These options are passed to `osbuild-mpp` as separate `-D` flags. Each of these options will be individually processed as JSON input, and therefore quotes must be escapaed or nested properly at the command line. Example:
    ```
    osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D image_type=\"regular\" -D extra_rpms=[\"dnf\",\"vim-enhanced\"]
    ```
    OR
    ```
    osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D 'image_type="regular"' -D 'extra_rpms=["dnf","vim-enhanced"]'
    ```

2. After preprocessing, pass the resolved manifest to `osbuild`, which builds the image in a series of steps. Example:
   ```
   sudo osbuild /tmp/output.json --store store/ --output-directory out/ --export qcow2
   ```

    When you run `osbuild` you can choose which steps to export. Typically, we export either the step called _image_ or the step called _qcow2_:

    * Image is a raw image that can be written to disk.
    * QCOW2 is a format used by QEMU for image files.

### Using makefile to build the image

You can simplify the build process by using makefile.

!!! note

     Makefile uses sudo, so you may be asked for your password during the build process.

1. From the `osbuild-manifest` directory, run `make`. Be sure to use the correct image name as the target:
    ```
    make cs9-qemu-minimal-regular.x86_64.qcow2
    ```
    This command preprocesses and builds the manifest for the current architecture and defines the image type to be `ostree`. This results in a file named `cs9-qemu-minimal-regular.x86_64.qcow2`, which is stored in the current directory.

2. You can use makefile to build RAW images as well. To see the full list of images available for the current architecture:
    ```
    make help
    ```
3. Optional: Remove the `_build` directory to regain disk space:
    ```
    rm -r _build
    ```
!!! note

    During the build process, artifacts such as JSON files, RPMs, and cached parts from previous builds are stored in the `_build` directory. Everything in this directory is derived from other sources.

### Changing the default password

The sample images use `password` as the default password for `root` and `guest`. You can change the default password when you build the image:
```
DEFINES='root_password="<hash_of_new_password>"'
```

## Running the image
You can either run the image in QEMU/KVM or flash the image onto an SD card.

### Booting the image in QEMU/KVM

Boot the QCOW2 image in `virt-manager` or run it directly through QEMU using the included tool (`runvm`):

If on MacOS, you must install qemu to use `runvm` via:

```
brew install qemu
```

`runvm` command:

```
./runvm cs9-qemu-minimal-regular.x86_64.qcow2
```

### Flashing the image onto an SD card

To flash the image onto an SD card:

```
dd if=cs9-qemu-minimal-regular.x86_64.img of=<SD card device path> status=progress bs=4M
```
!!! important

    You must change the value for `of=` to match the block device path used in your system (e.g., `/dev/sdb`). If required, add `-bios /usr/share/OVMF/OVMF_CODE.fd` to QEMU command.

## Using `make manifests`

You can use `make manifests` to preprocess all of the existing manifests without building them. You can use `make manifests` to verify that all combinations of options still work after a change or to inspect the resulting manifests.

In addition to the image types `*.img` and `*.qcow2`, the makefile also supports targets such as:

* `*.rootfs`
* `*.repo`
* `*.tar`
* `*.container`
* `*.ext4`

These targets are useful during development and testing. For more information, run `make help`.

## Building in a virtual machine

The makefile configuration supports running osbuild inside a virtual machine. There are two scenarios in which this is beneficial:

* Standard osbuild requires root/sudo permissions, because it performs some system-level operations such as loopback mounts. Running osbuild in a VM allows you to run it non-privileged.
* When you build an image for a different architecture, you can use QEMU software emulation to make this work. Software emulation is slower than native, but for some use cases it is sufficient.

Running `make osbuildvm-images` uses osbuild on the host to build the supporting image files, `_build/osbuildvm-*`. These files are required to build an image in a VM. The supporting image files can later be used to build other images from the same architecture inside a virtual machine by passing `VM=1` to `make`.

To build images from a different architecture in a VM, there are two options.

1. You may copy the output of `make osbuildvm-images` from a build on a different architecture into `_build`. For example, the contents of _build/osbuildvm-* from an aarch64 machine into _build/ on an x86_64 machine.

2. Alternatively, it is possible to use pre-built osbuildvm images. To build for aarch64 on an x86_64 host, follow the steps outlined below.

    Change to the osbuild-manifests directory
    ```
    cd sample-images/osbuild-manifests/
    ```
    Retrieve the nightly osbuildvm* files and place them into the _build folder
    ```
    wget -r -l 1 -nH -nd -np -A 'osbuildvm*' -P _build https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/
    ```

Once the _build folder is populated with the relevant `osbuildvm-*` files, you may then run `make` with a target that has an architecture different to the host system, for example, make `cs9-qemu-minimal-ostree.aarch64.qcow2`.

!!! note

     You can download pre-built osbuildvm-images files for the aarch64 architecture from the nightly folder at: https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/

!!! note

     If you have a previous version of `osbuildvm-images` for a different architecture, you can rebuild or refresh them by using `VM=1` on your architecture.

## Additional resources

For more information, see:

* [Updating an OSTree-based image](updating_ostree.md)
* [Customizing your OSBuild template](customize_template.md)
* [Embedding Containers in images](containers.md)
* [Encrypted images](encryption.md)
* [Using a Raspberry Pi as USB gadget](gadget.md)
* [Boot Time Optimization](boot_time_optimization.md)
* [Automotive Communities](automotive_communities.md)
